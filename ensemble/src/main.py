if __name__ == '__main__':
    import sys
    import pickle
    import os
    from decimal import Decimal
    from agent import Agent

    port = '5555'
    address = 'localhost'
    output_dir = '../output/'

    if (os.path.exists('/.dockerenv') or
       any('docker' in line for line in open('/proc/self/cgroup'))):
        address = 'environment'
        output_dir = '/output/'

    print("Using configuration")
    print("Address: " + address)
    print("Port: " + port, end="\n\n")

    episodes = 1E3
    trials = 30
    max_steps = 100
    total_steps = 0

    config = {'learning rate': Decimal(2)/Decimal(10),
              'discount': Decimal(9)/Decimal(10),
              'epsilon': Decimal(1)/Decimal(10)}

    returns = list()
    steps = list()
    policies = list()
    states = list()

    trial = 0
    while trial < trials:
        trial += 1
        print('*' * 30)
        print("Trial\t{:^5} of {:^5}".format(trial, trials))
        a = Agent(address, port, config)

        ret, step, pol, v_states = a.run_agent(episodes=episodes)

        returns.append(ret)
        steps.append(step)
        policies.append(pol)
        states.append(v_states)

        total_steps += sum(step)
        print(total_steps)

    with open(output_dir + 'returns.pck', 'wb') as f:
        pickle.dump(returns, f)

    with open(output_dir + 'steps.pck', 'wb') as f:
        pickle.dump(steps, f)

    with open(output_dir + 'policies.pck', 'wb') as f:
        pickle.dump(policies, f)

    with open(output_dir + 'states.pck', 'wb') as f:
        pickle.dump(states, f)

