from abc import ABCMeta, abstractmethod


class MetaAgent(metaclass=ABCMeta):

    # -------------------------------------------------------------------------
    # Communication related methods
    # -------------------------------------------------------------------------
    def __init__(self, address, port):
        import zmq

        addr = "tcp://{}:{}".format(address, port)
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect(addr)
        self.old_q = dict()
        self.q = dict()

    # -------------------------------------------------------------------------
    def send_data(self, data):
        self.socket.send_json(data)

    # -------------------------------------------------------------------------
    def recv_data(self):
        return self.socket.recv_json()

    # -------------------------------------------------------------------------
    def format_msg(self, init, state, action):
        return {'init': init,
                'state': state,
                'action': action
                }


    # -------------------------------------------------------------------------
    # Agent-Environment interaction related methods
    # -------------------------------------------------------------------------
    def run_agent(self, **kwargs):

        episode = 0

        returns = list()
        total_steps = list()
        policies = list()
        states = list()

        episodes = kwargs['episodes'] if 'episodes' in kwargs else 500
        max_steps = kwargs['max_steps'] if 'max_steps' in kwargs else 100

        while episode < episodes:
            episode += 1
            ret = 0
            steps = 0
            policy = list()
            visited = list()

            print("Episode\t{:>5} of {:>5}".format(episode, episodes))

            msg = self.format_msg(True, None, None)
            self.send_data(msg)
            env_msg = self.recv_data()

            self.init(env_msg)
            terminal = False

            while not terminal:
                steps += 1
                ret += env_msg['reward']

                state = env_msg['state']
                action = self.choose_action(env_msg)

                visited.append(state)
                policy.append(action)

                act_set = env_msg['actions']
                agt_msg = self.format_msg(False, state, action)
                self.send_data(agt_msg)
                env_msg = self.recv_data()

                agt_msg['actions'] = act_set
                self.do_something(agt_msg, env_msg)

                terminal = env_msg['terminal'] or steps >= max_steps

            ret += env_msg['reward']
            visited.append(env_msg['state'])
            returns.append(ret)
            total_steps.append(steps)
            policies.append(policy)
            states.append(visited)
            print("Return: {} Steps: {}".format(ret, steps))
            print("Policy:")
            print(policy)
            print("States:")
            print(visited)

        self.socket.close()

        return returns, total_steps, policies, states

    # -------------------------------------------------------------------------
    @abstractmethod
    def init(self, env_msg):
        pass

    # -------------------------------------------------------------------------
    @abstractmethod
    def choose_action(self, env_msg):
        pass

    # -------------------------------------------------------------------------
    @abstractmethod
    def do_something(self, agt_msg, env_msg):
        pass

    # -------------------------------------------------------------------------
