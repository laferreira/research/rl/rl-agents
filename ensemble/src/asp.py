from module import Module

class ASP(Module):

    # -------------------------------------------------------------------------
    def __init__(self):
        pass


    # -------------------------------------------------------------------------
    def init(self, env_msg):
        pass


    # -------------------------------------------------------------------------
    def update(self, agt_msg, env_msg):
        pass


    # -------------------------------------------------------------------------
    def get_action_values(self, state, actions):
        action_values = {action: 0 for action in actions}
        return action_values


    # -------------------------------------------------------------------------
    def print_gw_map(self):
        pass


    # -------------------------------------------------------------------------
