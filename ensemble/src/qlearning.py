from module import Module

class QLearning(Module):
    import pprint
    import json
    from operator import itemgetter
    from decimal import Decimal

    # -------------------------------------------------------------------------
    def __init__(self, learning_rate, discount):

        self.state = None
        self.q = dict()
        self.pp = self.pprint.PrettyPrinter(indent=2)
        self.lr = learning_rate
        self.disc = discount
        self.max_x = 0
        self.max_y = 0


    # -------------------------------------------------------------------------
    def init(self, env_msg):
        #print("-"*30)
        #print("Current table")
        #self.pp.pprint(self.q)
        #print("-"*30)
        pass


    # -------------------------------------------------------------------------
    def update(self, agt_msg, env_msg):
        s = self.get_state(agt_msg['state'])
        print(s)

        a = agt_msg['action']

        ns = env_msg['state']
        na = agt_msg['actions']

        max_a_Q = self.max_a_Q
        r = env_msg['reward']

        q = self.q
        α = self.lr
        γ = self.disc

        if s not in self.q:
            self.q[s] = dict()

        for action in na:
            if action not in self.q[s]:
                self.q[s][action] = self.Decimal(0)

        # from Sutton & Barto RL book 2ed
        q[s][a] += α * (r + γ * max_a_Q(ns, na) - q[s][a])


    # -------------------------------------------------------------------------
    def max_a_Q(self, state, actions):
        max_a = 0

        if self.get_state(state) in self.q:
            act_vals = self.get_action_values(state, actions)
            max_a =  max(act_vals.items(), key=self.itemgetter(1))[1]

        return max_a


    # -------------------------------------------------------------------------
    def get_action_values(self, state, actions):
        state = self.get_state(state)
        act_dict = {a: 0 for a in actions}

        if state in self.q:
            act_dict = {a: self.q[state][a] for a in actions}

        return act_dict

    # -------------------------------------------------------------------------
    def get_state(self, state):
        #s = self.json.dumps(state)                     # 1st try
        #s = tuple(state['pos'])                        # 2nd try
        keys = ['pos', 'up', 'down', 'left', 'right']   # 3rd try
        #keys = ['up', 'down', 'left', 'right']         # 4th try
        s = self.json.dumps({k:state[k] for k in keys}) # 3rd and 4th try
        return s
    # -------------------------------------------------------------------------
    def print_gw_map(self):
        action_char = {'up':'^', 'down':'v', 'left':'<', 'right':'>'}

        pi_star = list()
        vals = list()
        states = list()
        for s in self.q:
            x, y = self.json.loads(s)['pos']
            states.append((x,y))

            if x > self.max_x:
                self.max_x = x

            if y > self.max_y:
                self.max_y = y

            action = max(self.q[s].items(), key=self.itemgetter(1))[0]
            a = action_char[action]
            pi_star.append((x,y,a))
            av = ["{}:{}".format(action_char[k], round(v,0)) for k, v in self.q[s].items()]
            av = ' '.join(av)
            vals.append((x, y, av))

        for x in range(1, self.max_x+1):
            for y in range(1, self.max_y+1):
                if (x,y) not in states:
                    pi_star.append((x, y, '-'))


        pi_star.sort()
        vals.sort()

        c = 0
        for (x, y, a) in pi_star:
            if x > c:
                c += 1
                print()
            else:
                print(a, end=' ')



    # -------------------------------------------------------------------------
