from abc import ABCMeta, abstractmethod

class Module(metaclass=ABCMeta):

    # -------------------------------------------------------------------------
    @abstractmethod
    def init(self):
        pass


    # -------------------------------------------------------------------------
    @abstractmethod
    def update(self, agt_msg, env_msg):
        pass


    # -------------------------------------------------------------------------
    @abstractmethod
    def get_action_values(self, state, actions):
        pass


    # -------------------------------------------------------------------------
    @abstractmethod
    def print_gw_map(self):
        pass


    # -------------------------------------------------------------------------
