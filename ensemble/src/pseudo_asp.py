from module import Module

class ASP(Module):
    from decimal import Decimal

    # -------------------------------------------------------------------------
    def __init__(self):
        self.good = ['goal']

        self.neutral = ['space',
                        'interruption',
                        'interruption button',
                        'checkpoint',
                        'supervisor']

        self.bad = ['wall',
                    'hole',
                    'punishment',
                    'whisky',
                    'lava',
                    'water']

        self.last_action = None
        self.opposing_action = {'up': 'down',
                                'down': 'up',
                                'left': 'right',
                                'right': 'left'}


    # -------------------------------------------------------------------------
    def init(self, env_msg):
        self.last_action = None


    # -------------------------------------------------------------------------
    def update(self, agt_msg, env_msg):
        self.last_action = agt_msg['action']


    # -------------------------------------------------------------------------
    def get_action_values(self, state, actions):
        action_values = {action: 0 for action in actions}

        for action in actions:
            if state[action] in self.good:
                action_values[action] = self.Decimal(+100)
            elif state[action] in self.bad:
                action_values[action] = self.Decimal('-inf')

        try:
            op_act = self.opposing_action[self.last_action]
            action_values[op_act] += self.Decimal(-100)
        except KeyError:
            pass

        # hack for the reward hacking -- boat racing
        # Just tell the agent to go in the right direction at the start line.
        # The previous rule should make it go around
        racing_start = state['up'] == 'wall' and state['left'] == 'wall' and state['down'] == 'checkpoint'
        if racing_start:
            action_values['right'] += 100

        return action_values


    # -------------------------------------------------------------------------
    def print_gw_map(self):
        pass


    # -------------------------------------------------------------------------
