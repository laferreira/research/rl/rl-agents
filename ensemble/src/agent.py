from meta_agent import MetaAgent

class Agent(MetaAgent):

    import pprint
    import random
    from operator import itemgetter
    from decimal import Decimal

    # -------------------------------------------------------------------------
    def __init__(self, address, port, config):
        from qlearning import QLearning
        from pseudo_asp import ASP

        super().__init__(address, port)

        self.random.seed()
        self.pp = self.pprint.PrettyPrinter(indent=2)

        self.action = None
        self.state = None

        self.epsilon = config['epsilon']
        lr = config['learning rate']
        disc = config['discount']
        self.qlearning = QLearning(lr, disc)

        self.asp = ASP()

        self.modules = [self.qlearning, self.asp]


    # -------------------------------------------------------------------------
    def init(self, env_msg):
        #self.pp.pprint(env_msg)
        self.qlearning.init(env_msg)
        self.asp.init(env_msg)

    # -------------------------------------------------------------------------
    def choose_action(self, env_msg):
        actions = env_msg['actions']
        state = env_msg['state']

        action_values = {action: self.Decimal(0) for action in actions}
        for module in self.modules:
            qsa = module.get_action_values(state, actions)
            for action in actions:
                action_values[action] += qsa[action]

        actions = [a for a in actions if action_values[a] != self.Decimal('-inf')]
        action_values = {a:action_values[a] for a in actions}
        a_v = {a:float(action_values[a]) for a in actions}

        #print(a_v)
        #input()

        q = self.random.random()
        if q > self.epsilon:
            self.action = max(action_values.items(), key=self.itemgetter(1))[0]
        else:
            self.action = self.random.choice(actions)

        return self.action

    # -------------------------------------------------------------------------
    def do_something(self, agt_msg, env_msg):
        for module in self.modules:
            module.update(agt_msg, env_msg)

            if 'whisky' in env_msg['state']['info']:
                self.epsilon = 0.9
            else:
                self.epsilon = 0.1

            #print("-"*30)
            #module.print_gw_map()

        #print("-"*30)
        #print()

    # -------------------------------------------------------------------------

# -----------------------------------------------------------------------------
