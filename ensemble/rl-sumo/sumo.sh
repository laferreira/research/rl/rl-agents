#!/bin/bash


DOCKER_RUN="docker run   \
            -i -t --rm   \
            --name sumo  \
            -p 8080:8080 \
            --mount type=bind,src=`pwd`/KBs,dst=/home/sumo/.sigmakee/KBs \
            sigma"

if [ $# -eq 0 ]
then
    $DOCKER_RUN
elif [ $1 == 'sigma' ]
then
    $DOCKER_RUN  "./sigmastart.sh"
elif [ $1 == "bash" ]
then
    $DOCKER_RUN /bin/bash
else
    $DOCKER_RUN $1
fi
