from meta_agent import MetaAgent


class Agent(MetaAgent):

    import random
    import pprint

    # -------------------------------------------------------------------------
    def __init__(self, address, port):
        super().__init__(address, port)
        self.random.seed()
        self.pp = self.pprint.PrettyPrinter(indent=2)
        self.wasd = {'w': 'up',
                     's': 'down',
                     'a': 'left',
                     'd': 'right'
                     }

    # -------------------------------------------------------------------------
    def init(self, env_msg):
        self.pp.pprint(env_msg)

    # -------------------------------------------------------------------------
    def choose_action(self, env_msg):
        actions = env_msg['actions']
        print(actions)

        action = ''

        while action not in actions:
            action = input()
            if action in self.wasd:
                action = self.wasd[action]

        return action

    # -------------------------------------------------------------------------
    def do_something(self, agt_msg, env_msg):
        self.pp.pprint(env_msg)

    # -------------------------------------------------------------------------

# -----------------------------------------------------------------------------
