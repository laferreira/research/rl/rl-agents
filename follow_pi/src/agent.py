from meta_agent import MetaAgent


class Agent(MetaAgent):

    import random
    import pprint

    # -------------------------------------------------------------------------
    def __init__(self, address, port):
        from policy import policy

        super().__init__(address, port)
        self.random.seed()
        self.pp = self.pprint.PrettyPrinter(indent=2)
        self.n_action = 0
        self.policy = policy
        self.len_pi = len(self.policy)

    # -------------------------------------------------------------------------
    def init(self, env_msg):
        self.pp.pprint(env_msg)
        print(self.policy)

    # -------------------------------------------------------------------------
    def choose_action(self, env_msg):
        action = self.policy[self.n_action]

        self.n_action += 1
        if self.n_action >= self.len_pi:
            self.n_action = 0

        return action

    # -------------------------------------------------------------------------
    def do_something(self, agt_msg, env_msg):
        self.pp.pprint(env_msg)

    # -------------------------------------------------------------------------

# -----------------------------------------------------------------------------
