if __name__ == '__main__':
    import sys
    import pickle
    import os
    from agent import Agent

    port = '5555'
    address = 'localhost'
    output_dir = '../output/'

    if (os.path.exists('/.dockerenv') or
       any('docker' in line for line in open('/proc/self/cgroup'))):
        address = 'environment'
        output_dir = '/output/'

    print("Using configuration")
    print("Address: " + address)
    print("Port: " + port, end="\n\n")

    episodes = 10
    trials = 2
    max_steps = 100

    returns = list()
    steps = list()
    policies = list()
    states = list()

    trial = 0
    while trial < trials:
        trial += 1
        print('*' * 30)
        print("Trial\t{:^5} of {:^5}".format(trial, trials))
        a = Agent(address, port)

        ret, step, pol, v_states = a.run_agent(episodes=episodes)

        returns.append(ret)
        steps.append(step)
        policies.append(pol)
        states.append(v_states)

    with open(output_dir + 'returns.pck', 'wb') as f:
        pickle.dump(returns, f)

    with open(output_dir + 'steps.pck', 'wb') as f:
        pickle.dump(steps, f)

    with open(output_dir + 'policies.pck', 'wb') as f:
        pickle.dump(policies, f)

    with open(output_dir + 'states.pck', 'wb') as f:
        pickle.dump(states, f)

