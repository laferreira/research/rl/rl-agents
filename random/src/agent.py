from meta_agent import MetaAgent


class Agent(MetaAgent):

    import random
    import pprint

    # -------------------------------------------------------------------------
    def __init__(self, address, port):
        super().__init__(address, port)
        self.random.seed()
        self.pp = self.pprint.PrettyPrinter(indent=2)

    # -------------------------------------------------------------------------
    def init(self, env_msg):
        self.pp.pprint(env_msg)

    # -------------------------------------------------------------------------
    def choose_action(self, env_msg):
        actions = env_msg['actions']
        action =  self.random.choice(actions)

        print(action)
        return action

    # -------------------------------------------------------------------------
    def do_something(self, agt_msg, env_msg):
        self.pp.pprint(env_msg)

    # -------------------------------------------------------------------------

# -----------------------------------------------------------------------------
